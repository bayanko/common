package util

import (
	"net/http"

	responseModel "gitlab.com/bayanko/common/model/response"

	"github.com/gin-gonic/gin"
)

type response struct{}

type IResponseCreator interface {
	Created(ctx *gin.Context, model interface{})
	InternalServerError(ctx *gin.Context, err error)
	UnprocessableEntity(ctx *gin.Context, err error)
	Ok(ctx *gin.Context)
	NoContent(ctx *gin.Context)
	NotFound(ctx *gin.Context)
	OkWithData(ctx *gin.Context, model interface{})
}

func ProvideResponseCreator() IResponseCreator {
	return &response{}
}

func (r *response) OkWithData(ctx *gin.Context, model interface{}) {
	ctx.JSON(http.StatusOK, model)
}

func (r *response) InternalServerError(ctx *gin.Context, err error) {
	ctx.JSON(http.StatusInternalServerError, &responseModel.Response{Error: err.Error()})
}

func (r *response) UnprocessableEntity(ctx *gin.Context, err error) {
	ctx.JSON(http.StatusUnprocessableEntity, &responseModel.Response{Error: err.Error()})
}

func (r *response) Ok(ctx *gin.Context) {
	ctx.Status(http.StatusOK)
}

func (r *response) NoContent(ctx *gin.Context) {
	ctx.Status(http.StatusNoContent)
}

func (r *response) NotFound(ctx *gin.Context) {
	ctx.Status(http.StatusNotFound)
}

func (r *response) Created(ctx *gin.Context, model interface{}) {
	ctx.JSON(http.StatusCreated, model)
}
