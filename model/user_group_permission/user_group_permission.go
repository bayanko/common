package user_group_permission

import (
	userModuleModel "gitlab.com/bayanko/common/model/user_module"
	userPermissionModel "gitlab.com/bayanko/common/model/user_permission"
)

type UserGroupPermission struct {
	UserPermission *userPermissionModel.UserPermission `json:"userPermission,omitempty"`
	UserModule     *userModuleModel.UserModule         `json:"userModule,omitempty"`
}
