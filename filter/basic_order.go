package filter

type BasicOrder struct {
	// Field which you want to order by
	OrderBy  string `form:"orderBy" schema:"order_by"`
	OrderDir string `form:"orderDir" schema:"order_dir"`
}

// NewBasicOrder ...
func NewBasicOrder() *BasicOrder {
	return &BasicOrder{}
}

func (s *BasicOrder) GetOrderBy() string {
	if s.OrderBy != "" {
		if s.OrderDir == "" {
			s.OrderDir = "ASC"
		}
		return s.OrderBy + " " + s.OrderDir
	}

	return ""
}
