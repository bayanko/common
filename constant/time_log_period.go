package constant

type TimeLogPeriod string

const (
	FirstWeek  TimeLogPeriod = "1"
	SecondWeek TimeLogPeriod = "2"
	ThirdWeek  TimeLogPeriod = "3"
	FourthWeek TimeLogPeriod = "4"
	EndOfMonth TimeLogPeriod = "EOM"
	EndOfYear  TimeLogPeriod = "EOY"
)

var TimeLogPeriods = map[string]TimeLogPeriod{
	"1":   FirstWeek,
	"2":   SecondWeek,
	"3":   ThirdWeek,
	"4":   FourthWeek,
	"EOM": EndOfMonth,
	"EOY": EndOfYear,
}
