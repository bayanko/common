package constant

type AppMode string
type GinMode string

const (
	Develop    AppMode = "DEVELOP"
	Staging    AppMode = "STAGING"
	UAT        AppMode = "UAT"
	Production AppMode = "PRODUCTION"
)

const (
	GinModeDebug   GinMode = "debug"
	GinModeRelease GinMode = "release"
)
