package migration

import (
	"flag"
	"fmt"
	"log"
	"os"

	"gitlab.com/bayanko/common/config"
	"gitlab.com/bayanko/common/connection"

	"github.com/kelseyhightower/envconfig"

	"github.com/pressly/goose"
)

var (
	flags = flag.NewFlagSet("goose", flag.ExitOnError)
	dir   = flags.String("dir", "./internal/cmd/migration/sql", "directory with migration files")

	usagePrefix = `
		Usage: migrate [OPTIONS] COMMAND
			Options:`

	usageCommands = `
		Commands:
			up         Migrate the DB to the most recent version available
			up-by-one  Migrate the DB to the next available version
			down       Roll back the version by 1
			refresh    Down all migrations and apply them again
			status     Dump the migration status for the current DB
			version    Print the current version of the database
			create     Creates a blank migration template`

	usageCreate = `create must be of form: migrate [OPTIONS] create NAME [go|sql|gorm]`
)

func RunMigration() {
	flags.Usage = usage
	flags.Parse(os.Args[1:])

	args := flags.Args()
	if len(args) < 1 {
		flags.Usage()
		return
	}

	command := args[0]

	if command == "-h" || command == "--help" {
		flags.Usage()
		return
	}

	appConf := config.ProvideAppConfig()

	if err := goose.SetDialect("postgres"); err != nil {
		panic(err.Error())
	}

	var postgresConfig config.PostgresConfig

	envconfig.MustProcess(string(appConf.AppMode), &postgresConfig)

	dbConfig := config.PostgresConfig{
		Host:     postgresConfig.Host,
		Port:     postgresConfig.Port,
		Name:     postgresConfig.Name,
		User:     postgresConfig.User,
		Password: postgresConfig.Password,
	}

	db := connection.ProvidePostgresConnection(&dbConfig)
	sqlDb, err := db.DB()
	if err != nil {
		panic(err.Error())
	}

	if err := goose.Run(command, sqlDb, *dir, args[1:]...); err != nil {
		log.Fatalf("goose run: %v", err)
	}
}

func usage() {
	fmt.Print(usagePrefix)
	flags.PrintDefaults()
	fmt.Print(usageCommands)
	fmt.Print(usageCreate)
}
