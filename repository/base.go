package repository

import (
	"fmt"
	"time"

	filter "gitlab.com/bayanko/common/filter"

	"gorm.io/gorm"
)

type BaseRepository struct {
	DB *gorm.DB
}

type IBaseRepository interface {
	Create(interface{}) error
	Update(interface{}) error
	FindByID(string, interface{}, ...string) error
	Delete(string, interface{}) error
	SoftDelete(string, interface{}) error
	FetchByFilter(interface{}, filter.Filter) error
	Transaction() ITransaction
}

func ProvideBaseRepository(transaction ITransaction, db *gorm.DB) *BaseRepository {
	return &BaseRepository{
		DB: db,
	}
}

func (b *BaseRepository) Transaction() ITransaction {
	return &Transaction{DB: b.DB}
}

func (b *BaseRepository) Create(m interface{}) error {
	return b.DB.Model(m).Create(m).Error
}

func (b *BaseRepository) Update(m interface{}) error {
	return b.DB.Updates(m).Error
}

func (b *BaseRepository) FindByID(id string, model interface{}, preloads ...string) error {
	db := b.DB.Where("id = ?", id).Where("deleted_at IS NULL")

	for _, p := range preloads {
		db = db.Preload(p)
	}

	err := db.Take(model).Error

	if err != nil {
		if err.Error() == gorm.ErrRecordNotFound.Error() {
			return nil
		}
	}

	return err
}

func (b *BaseRepository) Delete(id string, m interface{}) error {
	return b.DB.Delete(m, "id = ?", id).Error
}

func (b *BaseRepository) SoftDelete(id string, m interface{}) error {
	err := b.DB.Model(m).Where("id = ?", id).Update("deleted_at", time.Now()).Error
	if err == gorm.ErrRecordNotFound {
		return nil
	}
	return err
}

func (b *BaseRepository) FetchByFilter(val interface{}, f filter.Filter) error {
	q := b.DB.Model(val)

	for key, query := range f.GetJoins() {
		q = q.Joins(key)
		for _, where := range query {
			q = q.Where(fmt.Sprintf("\"%s\".%v", key, where))
		}
	}

	for _, val := range f.GetGroupBy() {
		q = q.Group(val)
	}

	for query, val := range f.GetWhere() {
		q = q.Where(query, val...)
	}

	if len(f.GetOrderBy()) > 0 {
		q = q.Order(f.GetOrderBy())
	}

	if f.GetLimit() > 0 {
		q = q.Limit(f.GetLimit())
	}
	q = q.Offset(f.GetOffset())

	for key, val := range f.GetPreload() {
		q = q.Preload(key, val...)
	}

	return q.Find(val).Error
}
