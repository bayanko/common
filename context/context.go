package util

import (
	"errors"
	"net/http"
	"strings"

	config "gitlab.com/bayanko/common/config"
	userModel "gitlab.com/bayanko/common/model/user"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

type context struct {
	AppConfig *config.AppConfig
}

type IContext interface {
	ACL(userModule string) gin.HandlerFunc
}

func ProvideContextUtil(appConfig *config.AppConfig) IContext {
	return &context{AppConfig: appConfig}
}

func (c *context) ACL(userModule string) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		fullPath := ctx.FullPath()

		bearerString := ctx.GetHeader("Authorization")
		if bearerString == "" {
			ctx.JSON(http.StatusForbidden, errors.New("invalid autorization header"))
			ctx.Abort()
			return
		}

		tokenString := strings.Split(bearerString, " ")[1]
		mySigningKey := []byte(c.AppConfig.APIKey)

		token, err := jwt.ParseWithClaims(tokenString, &userModel.UserAccessTokenCustomClaims{}, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, errors.New("unexpected signing method")
			}

			return mySigningKey, nil
		})

		if err != nil {
			ctx.JSON(http.StatusForbidden, err)
			ctx.Abort()
			return
		}

		claims := token.Claims.(*userModel.UserAccessTokenCustomClaims)

		ctx.AddParam("userID", claims.ID)
		ctx.AddParam("username", claims.Username)
		ctx.AddParam("email", claims.Email)

		if _, isPresent := claims.ACL[fullPath]; !isPresent {
			ctx.JSON(http.StatusForbidden, errors.New("unauthorized access"))
			ctx.Abort()
			return
		}

		if !strings.Contains(claims.ACL[fullPath], userModule) {
			ctx.JSON(http.StatusForbidden, errors.New("unauthorized access"))
			ctx.Abort()
			return
		}

		ctx.Next()
	}
}
