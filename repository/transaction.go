package repository

import "gorm.io/gorm"

type Transaction struct {
	DB *gorm.DB
}

type ITransaction interface {
	Create() *gorm.DB
}

func NewTransaction(db *gorm.DB) ITransaction {
	return &Transaction{DB: db}
}

func (b *Transaction) Create() *gorm.DB {
	return b.DB.Begin()
}
