package filter

const (
	defaultPerPage = 50
	maxPerPage     = 200
)

type PaginationFilter struct {
	BasicFilter
	BasicOrder

	// Page Number - Default is 1
	Page int `form:"page" schema:"page"`

	// Number of items per page - Default: 50, Max: 200
	PerPage int `form:"perPage" schema:"per_page"`

	IgnorePerPage bool `form:"ignorePerPage" schema:"ignore_per_page"`
}

func NewPaginationFilter() PaginationFilter {
	return PaginationFilter{
		BasicFilter: *NewBasicFilter(),
	}
}

// implement repository.Filter interface
func (f *PaginationFilter) GetLimit() int {
	return f.GetPerPage()
}

// implement repository.Filter interface
func (f *PaginationFilter) GetOffset() int {
	return (f.GetPage() - 1) * f.GetPerPage()
}

func (f *PaginationFilter) GetPage() int {
	if f.Page < 1 {
		return 1
	}
	return f.Page
}

func (f *PaginationFilter) GetPerPage() int {
	if f.IgnorePerPage {
		return 0
	}

	if f.PerPage < 1 || f.PerPage > maxPerPage {
		return defaultPerPage
	}

	return f.PerPage
}
