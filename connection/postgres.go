package connection

import (
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/bayanko/common/config"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

func getURL(postgreConfig *config.PostgresConfig) string {
	return fmt.Sprintf(
		"host=%s user=%s port=%d dbname=%s sslmode=disable password=%s",
		postgreConfig.Host,
		postgreConfig.User,
		postgreConfig.Port,
		postgreConfig.Name,
		postgreConfig.Password,
	)
}

func ProvidePostgresConnection(p *config.PostgresConfig) *gorm.DB {
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold:             time.Second, // Slow SQL threshold
			LogLevel:                  logger.Info, // Log level
			IgnoreRecordNotFoundError: false,       // Ignore ErrRecordNotFound error for logger
			Colorful:                  true,        // Disable color
		},
	)

	db, err := gorm.Open(postgres.Open(getURL(p)), &gorm.Config{
		Logger: newLogger,
	})
	if err != nil {
		panic(err)
	}

	return db
}
