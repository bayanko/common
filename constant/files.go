package constant

type FileType string

const (
	Image    FileType = "image"
	Video    FileType = "video"
	Audio    FileType = "audio"
	Document FileType = "document"
)

var FileTypes = map[string]FileType{
	"IMAGE":    Image,
	"VIDEO":    Video,
	"AUDIO":    Audio,
	"DOCUMENT": Document,
}
