package user

import (
	"github.com/dgrijalva/jwt-go"
)

type UserVerifyCustomClaims struct {
	Username  string `json:"username"`
	UserGroup string `json:"userGroup"`
	jwt.StandardClaims
}

type UserForgotPasswordCustomClaims struct {
	Username  string `json:"username"`
	UserGroup string `json:"userGroup"`
	jwt.StandardClaims
}

type UserRefreshTokenCustomClaims struct {
	ID string `json:"id"`
	jwt.StandardClaims
}

type UserAccessTokenCustomClaims struct {
	ID       string            `json:"id"`
	Username string            `json:"username"`
	Email    string            `json:"email"`
	ACL      map[string]string `json:"acl"`
	jwt.StandardClaims
}
