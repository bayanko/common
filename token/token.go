package token

import (
	"encoding/json"
	"fmt"

	"gitlab.com/bayanko/common/config"

	userGroupPermissionModel "gitlab.com/bayanko/common/model/user_group_permission"

	userModel "gitlab.com/bayanko/common/model/user"

	"github.com/dgrijalva/jwt-go"
)

// import (
// 	"encoding/base64"
// 	"errors"
// 	"net/http"
// 	"time"
// 	"user/internal/config"
// 	"user/internal/constant"
// 	"user/internal/model"

// 	"github.com/dgrijalva/jwt-go"
// 	jwtMiddleware "github.com/iris-contrib/middleware/jwt"
// 	"github.com/kataras/iris/v12"
// )

type token struct {
	AppConfig *config.AppConfig
}

type IToken interface {
	GenerateAccessToken(id, username string, email string, acl map[string]string, expiration int64) (string, error)
	GenerateRefreshToken(id string, expiration int64) (string, error)
	GenerateACLOBject(payload interface{}) map[string]string
	// GenerateAPIKey() string
	// GenerateAPIToken(apiKey string) (string, error)
	// APITokenChecker() *jwtMiddleware.GinJWTMiddleware
}

func ProvideTokenUtil(appConfig *config.AppConfig) IToken {
	return &token{AppConfig: appConfig}
}

func (t *token) GenerateACLOBject(payload interface{}) map[string]string {
	acls := make(map[string]string)

	bytes, _ := json.Marshal(payload)

	var userGroupPermissions []userGroupPermissionModel.UserGroupPermission
	json.Unmarshal(bytes, &userGroupPermissions)

	for _, userGroupPermission := range userGroupPermissions {
		acls[userGroupPermission.UserModule.Path] = fmt.Sprintf("%s%s", acls[userGroupPermission.UserModule.Path], userGroupPermission.UserPermission.Serial)
	}

	return acls
}

func (t *token) GenerateAccessToken(id, username string, email string, acl map[string]string, expiration int64) (string, error) {
	mySigningKey := []byte(t.AppConfig.APIKey)

	accessToken := &userModel.UserAccessTokenCustomClaims{
		ID:       id,
		Username: username,
		Email:    email,
		ACL:      acl,
		StandardClaims: jwt.StandardClaims{
			// 1hr
			ExpiresAt: expiration,
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, accessToken)

	tokenString, err := token.SignedString(mySigningKey)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func (t *token) GenerateRefreshToken(id string, expiration int64) (string, error) {
	mySigningKey := []byte(t.AppConfig.APIKey)

	accessToken := &userModel.UserRefreshTokenCustomClaims{
		ID: id,
		StandardClaims: jwt.StandardClaims{
			// 30 days
			ExpiresAt: expiration,
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, accessToken)

	tokenString, err := token.SignedString(mySigningKey)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

// func (t *token) GenerateAPIKey() string {
// 	return time.Now().Format("2006-01-02")
// }

// func (t *token) GenerateAPIToken(apiKey string) (string, error) {
// 	timeNow := time.Now().Format("2006-01-02")
// 	apiKeyNow := base64.StdEncoding.EncodeToString([]byte(timeNow))

// 	if apiKey != apiKeyNow {
// 		return "", errors.New("invalid api key")
// 	}

// 	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
// 		// set the expire time
// 		// see http://tools.ietf.org/html/draft-ietf-oauth-json-web-token-20#section-4.1.4
// 		ExpiresAt: time.Now().Add(time.Hour * 1).Unix(),
// 	})

// 	apiToken, err := token.SignedString([]byte(apiKey))
// 	if err != nil {
// 		return "", err
// 	}

// 	return apiToken, nil

// }

// func (t *token) APITokenChecker() *jwtMiddleware.Middleware {
// 	jwtHandler := jwtMiddleware.New(jwtMiddleware.Config{
// 		ValidationKeyGetter: func(token *jwtMiddleware.Token) (interface{}, error) {
// 			timeNow := time.Now().Format("2006-01-02")
// 			apiKey := base64.StdEncoding.EncodeToString([]byte(timeNow))

// 			return []byte(apiKey), nil
// 		},
// 		ErrorHandler:  jwtError,
// 		SigningMethod: jwtMiddleware.SigningMethodHS256,
// 	})

// 	return jwtHandler
// }

// func jwtError(ctx iris.Context, err error) {
// 	if err == nil {
// 		return
// 	}

// 	response := model.Response{
// 		Error: err.Error(),
// 		Code:  constant.ResponseStatusErrors[constant.InvalidToken],
// 	}

// 	ctx.StatusCode(http.StatusUnauthorized)
// 	_, _ = ctx.JSON(response)
// 	ctx.StopExecution()
// }
