package filter

const (
	// IgnoreLimit don't use LIMIT
	IgnoreLimit = -1
	// IgnoreOffset don't use OFFSET
	IgnoreOffset = -1
)

// Where ...
type Where map[string][]interface{}

// Preload ...
type Preload map[string][]interface{}

// Joins ...
type Joins map[string][]string

// Groups ...
type Groups []string

// Filter ...
type Filter interface {
	GetWhere() Where
	GetPreload() Preload
	GetJoins() Joins
	GetGroupBy() Groups
	GetLimit() int
	GetOffset() int
	GetOrderBy() string
}
